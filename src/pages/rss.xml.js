import rss from "@astrojs/rss";
import { getCollection } from "astro:content";

export async function GET(context) {
    const posts = await getCollection("writings");
    return rss({
        title: "Wahid Islam Linad",
        description:
            "I, Linad, am a developer with a mindset To create aesthetic and perfect programs throughout my lifespan.",
        site: context.site,
        items: posts.map((post) => ({
            ...post.data,
            link: `/writings/${post.slug}/`,
        })),
    });
}
