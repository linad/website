import { defineCollection, z } from "astro:content";

const writings = defineCollection({
    type: "content",
    // Type-check frontmatter using a schema
    schema: z.object({
        title: z.string(),
        description: z.string(),
        // Transform string to Date object
        pubDate: z.coerce.date(),
        updatedDate: z.coerce.date().optional(),
        ogImage: z.string().optional(),
        readTime: z.string().optional(),
    }),
});

export const collections = { writings };
