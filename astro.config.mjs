import { defineConfig } from "astro/config";
import mdx from "@astrojs/mdx";
import sitemap from "@astrojs/sitemap";
import tailwind from "@astrojs/tailwind";
import robotsTxt from "astro-robots-txt";
import rehypePrettyCode from "rehype-pretty-code";
import rehypeSlug from "rehype-slug";
import rehypeAutolinkHeadings from "rehype-autolink-headings";
import { remarkReadingTime } from "./src/utils/calculate-reading-time.mjs";
import icon from "astro-icon";

import celestial from "./src/lib/celestial.json";
const prettyCodeOptions = {
    theme: {
        dark: celestial,
        light: "github-light",
    },
};

const autoHeadOptions = {
    properties: {
        class: ["anchor"],
    },
};

export default defineConfig({
    site: "https://linad.me",
    markdown: {
        syntaxHighlight: false,
        remarkPlugins: [remarkReadingTime],
        rehypePlugins: [
            rehypeSlug,
            [rehypeAutolinkHeadings, autoHeadOptions],
            [rehypePrettyCode, prettyCodeOptions],
        ],
    },
    integrations: [
        mdx(),
        sitemap({
            filter: (page) => !page.startsWith("https://linad.me/rss.xml"),
            changefreq: "weekly",
            priority: 0.7,
            lastmod: new Date(),
        }),
        tailwind(),
        robotsTxt(),
        icon(),
    ],
});
